package com.slavamasa.autoupdatingnotificator

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.*
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat

class NotificationService : Service() {

    private var count: Int = 0
    private lateinit var notifier: Notifier
    private val handler = Handler(Looper.getMainLooper())

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, NotificationService::class.java)
            ContextCompat.startForegroundService(context, intent)
        }
    }

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        Log.d("rofl", "onCreate")
        notifier = Notifier(this)
        startForeground(Notifier.notificationId, notifier.notificationBuilder.build())
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("rofl", "onStartCommand")
        val delay = 1000L
        handler.postDelayed(object: Runnable {
                override fun run() {
                    Log.d("rofl", "$count")
                    if (count == 100) count = -1
                    count++
                    updateNotificationText()
                    handler.postDelayed(this, delay)
                }
            },
            delay
        )

        return START_REDELIVER_INTENT
    }

    override fun onDestroy() {
        Log.d("rofl", "onDestroy service")
        super.onDestroy()

    }

    private fun updateNotificationText() {
        notifier.updateNotification(count)
    }
}