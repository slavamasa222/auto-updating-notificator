package com.slavamasa.autoupdatingnotificator

import android.app.Application
import android.util.Log
import androidx.core.app.NotificationChannelCompat
import androidx.core.app.NotificationManagerCompat

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        setupNotificationChannel()
    }

    private fun setupNotificationChannel() {
        try {
            val notificationService = NotificationManagerCompat.from(this)
            Log.d("rofl", Notifier.notificationChannelId)
            val notificationChannel = NotificationChannelCompat.Builder(
                Notifier.notificationChannelId,
                NotificationManagerCompat.IMPORTANCE_HIGH
            ).setName(Notifier.notificationChannelId)
                .build()
            notificationService.createNotificationChannel(notificationChannel)
        } catch (e: Exception) {
            Log.e("rofl", "Notification channels + $e")
        }
    }
}