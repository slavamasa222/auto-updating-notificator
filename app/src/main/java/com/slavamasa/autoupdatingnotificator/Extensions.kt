package com.slavamasa.autoupdatingnotificator

import android.app.NotificationManager
import android.content.Context
import androidx.core.content.getSystemService

val Context.notificationManager: NotificationManager
    get() = getSystemService()!!