package com.slavamasa.autoupdatingnotificator

import android.content.Context
import androidx.core.app.NotificationCompat


class Notifier(private val context: Context) {

    companion object {
        const val notificationChannelId = "notification_channel"
        const val notificationId = 1
    }

    val notificationBuilder by lazy {
        NotificationCompat.Builder(context, notificationChannelId)
            .setForegroundServiceBehavior(NotificationCompat.FOREGROUND_SERVICE_IMMEDIATE)
            .setContentTitle("New Message")
            .setContentText("You've received new messages.")
            .setSmallIcon(R.drawable.ic_notification)
            .setAutoCancel(true)
            .setOnlyAlertOnce(true)
    }

    fun updateNotification(progress: Int) {
        context.notificationManager.notify(
            notificationId,
            notificationBuilder
                .setProgress(100, progress, false)
                .build()
        )
    }
}