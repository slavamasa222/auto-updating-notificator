package com.slavamasa.autoupdatingnotificator

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.slavamasa.autoupdatingnotificator.databinding.ActivityMainBinding
import java.lang.Exception

//https://stackoverflow.com/questions/46996052/how-to-make-handler-always-run-although-application-closed-in-java
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var notificationNumber = 0
    private var isNotified = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)



        val intent = Intent(this, NotificationService::class.java)

        binding.btnSendNotification.setOnClickListener {

            NotificationService.start(applicationContext)

            notificationNumber++
            Log.d("rofl", "btnSendNotification.setOnClickListener")


        }
    }
}